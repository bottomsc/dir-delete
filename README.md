# Dir-Delete

Directory deletion utility using dates embedded in directory names  

# Version

0.0.16

# Synopsis

For recurring deletion tasks, for example every day at 7:13AM deleting files older than a year (and sending yourself an email about it), you could set up crontab with something like:  

    13     7    *   *    *    /path/to/perl6 /path/to/bin/by_name_remove_dir_older_than --years=1 --force |& mail -s "Deletion report" name@example.com 

# Dependencies:  

Linux operating system (tested on CentOS 7)  

Rakudo Star (tested on 2017.07 and 2017.10)  

# TODO

Create function that exclusively gives a list of directories that do not meet the date cutoff.

# CHANGES

0.0.16: Added dry run option  
0.0.15: can now run in debug mode  
0.0.14: Can now specify multiple delete patterns  
0.0.13: adding test for Thumbnails directory  
0.0.12: Now deleting same pattern in two different subdirectories  
0.0.11: Now culling one level of sub directories fine  
0.0.10: Now also testing for YYMMDD$ style top dir names  
0.0.9: Internal code simplification  
0.0.8: Update TODO list  
0.0.7: added test file for not-yet implemented sudirectory culling functionality  
0.0.6: Update TODO list  
0.0.5: renamed test appropriately  
0.0.4: ignore precompilation files  
0.0.3: Improving documentation
0.0.2: Adding README file
