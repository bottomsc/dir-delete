# Dir-Delete

Directory deletion utility using dates embedded in directory names  

# Version

VERSION-PLACEHOLDER

# Synopsis

For recurring deletion tasks, for example every day at 7:13AM deleting files older than a year (and sending yourself an email about it), you could set up crontab with something like:  

    13     7    *   *    *    /path/to/perl6 /path/to/bin/by_name_remove_dir_older_than --years=1 --force |& mail -s "Deletion report" name@example.com 

# Dependencies:  

Linux operating system (tested on CentOS 7)  

Rakudo Star (tested on 2017.07 and 2017.10)  

# TODO

Create function that exclusively gives a list of directories that do not meet the date cutoff.

# CHANGES

