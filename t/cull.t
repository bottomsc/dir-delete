#!/bin/env perl6
use Test;

my $DEBUG;

my $EXE = "../../bin/cull-subdirectories";

chdir 't';

my $TODAY = '171114'; # November 14, 2017

# Month of the year and whether the 15th of that month should be retained
my %SHOULD-BE-RETAINED = (
    5 => False,
    6 => False,
    7 => False,
    8 => True,
    9 => True,
);

my @TOP-LEVEL-SUBDIRS = <Thumbnail_foobar Images Logs>;
my @DELETE-PATTERNS   = <L00 Thumb Images Logs>;

my $DELETE-PATTERN-ARGS = 
    '--delete-pattern=' ~ @DELETE-PATTERNS.join(" --delete-pattern=");

sub MAIN ($debug=False) {
    $DEBUG = $debug;
    test-format('YYMMDD');
    test-format('YYMMDD$','foo_bar_baz_');
    done-testing;
}

sub test-format ($format, $prefix='') {

    my $starting-dir = $*CWD;

    my $pattern = 'L00'; 

    my $temp-dir = 'temp-dir';

    mkdir $temp-dir;

    chdir $temp-dir;


    for %SHOULD-BE-RETAINED.kv -> $month, $expected {
        my $month-dir = "{$prefix}170{$month}15";

        subtest "Checked $month-dir", {

            my @subdir = 'foo/bar','foo/bar/baz';

            my @dirs-to-check;
            for @subdir -> $subdir {
                mkdir "$month-dir/$subdir/$pattern" ~ $_ for 1 .. 4;
                @dirs-to-check.append(dir("$month-dir/$subdir", test => / ^ "$pattern" .* / )); 
            }

            my @extra-dirs = @TOP-LEVEL-SUBDIRS.map: { "$month-dir/$_".IO };

            mkdir($_) for @extra-dirs;

            @dirs-to-check.append(@extra-dirs.flat);


            my $command =
                   "$EXE "
                 ~ " --force "
                 ~ " --time-format=$format "
                 ~ " $DELETE-PATTERN-ARGS "
                 ~ " --current-date-override=$TODAY "
                 ~ " --months=3 "
                 ~ " . "
                 ~ @subdir.join(" ")
                 ; 

            if $DEBUG {
                $command ~= ~ ' 1> cull_debug.o 2> cull_debug.e'; # capture standard out and error streams
            }
            else {
                $command ~= ~ ' 2> /dev/null'; # suppress standard error messages by sending them to /dev/null
            }

            shell $command;

            for @dirs-to-check -> $dir {
                my $result = so $dir.d; # Whether this directory still exists
                my $message = $expected ?? "$dir skipped" !! "$dir deleted";
                  
                is $result, $expected, $message;
            }
        }
    }

    chdir $starting-dir;
    
    delete-temp-dir($temp-dir) unless $DEBUG;
}

sub delete-temp-dir ($temp-dir) {
    shell "rm -rf $temp-dir";
    ok ! $temp-dir.IO.e, "Temp data directory deleted"; 
    return;
}

# vim: ft=perl6
