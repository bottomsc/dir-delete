#!/bin/env perl6
use Test;

my $exe = "../bin/by_name_remove_dir_older_than";

chdir 't';

my $temp-dir = 'test_delete_dir';

my $TODAY = '171214'; # December 14, 2017

test-format('YYMMDD');
test-format('YYMMDD$','foo_bar_baz_');

done-testing;

sub test-format ($format, $prefix='') {

    my $temp-dir = 'test_delete_dir';

    mkdir "$temp-dir/{$prefix}{$_}0115" for 10 .. 20;
    
    shell "$exe --format=$format --current-date-override=$TODAY --years=1 $temp-dir";
    
    my @remaining-dirs = dir($temp-dir);
    
    is @remaining-dirs.elems, 4, "format '$format' worked on dirs named like '{$prefix}171225'";
    
    delete-temp-dir($temp-dir);
}


sub delete-temp-dir ($temp-dir) {
    shell "rm -rf $temp-dir";
    ok ! $temp-dir.IO.e, "Temp data directory deleted"; 
    return;
}
